/*
  cropwidget
  Copyright (C) 2009  Rafał Rzepecki <divided.mind@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "cropwidget.h"

#include <QStackedLayout>
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QResizeEvent>

class CropWidgetPrivate
{
  public:
    CropWidgetPrivate(CropWidget *q): q_ptr(q) {}
    QPoint toContents(QPoint p) const;

    bool _snapped;
    QWidget *innerWidget;
    QPixmap snapshot;
    QPoint cropStart;
    QRect cropRect;

    CropWidget *q_ptr;

    Q_DECLARE_PUBLIC(CropWidget)
};

CropWidget::CropWidget(QWidget *parent): QFrame(parent), d_ptr(new CropWidgetPrivate(this))
{
  Q_D(CropWidget);
  d->_snapped = false;
}

CropWidget::~CropWidget() {
  delete d_ptr;
}

QPixmap CropWidget::snapshot() const
{
  Q_D(const CropWidget);
  if (d->_snapped)
    return d->snapshot;
  else
    return QPixmap();
}

void CropWidget::snap(bool snapped)
{
  Q_D(CropWidget);
  d->cropStart = QPoint();
  d->cropRect = QRect();
  const QList<QWidget *> widgets = findChildren<QWidget *>();
  if (snapped) {
    d->snapshot = QPixmap::grabWidget(this, contentsRect());
    foreach(QWidget *w, widgets)
      w->hide();
  } else {
    foreach(QWidget *w, widgets)
      w->show();
    update();
  }
  d->_snapped = snapped;
}

static QRect resizeAspect(QRect target, QSize size)
{
  size.scale(target.size(), Qt::KeepAspectRatio);
  QRect newRect(QPoint(), size);
  newRect.moveCenter(target.center());
  return newRect;
}

void CropWidget::paintEvent(QPaintEvent *event)
{
  Q_D(CropWidget);

  QFrame::paintEvent(event);
  QPainter painter(this);
  if (!d->_snapped) {
    painter.eraseRect(contentsRect());
    return;
  }

  painter.setRenderHint(QPainter::SmoothPixmapTransform);
  painter.drawPixmap(resizeAspect(contentsRect(), d->snapshot.size()), d->snapshot);

  if (d->cropRect.isValid()) {
    QImage img(contentsRect().size(), QImage::Format_ARGB32);
    QPainter ppm(&img);
    ppm.fillRect(rect(), QColor(255, 255, 255, 200));    ppm.setCompositionMode(QPainter::CompositionMode_Clear);
    ppm.eraseRect(d->cropRect);
    painter.drawImage(contentsRect(), img);
  }

  event->accept();
}

void CropWidget::crop()
{
  Q_D(CropWidget);

  if ((!d->_snapped) || (!d->cropRect.isValid()))
    return;

  int x = d->cropRect.x() * d->snapshot.width() / width();
  int y = d->cropRect.y() * d->snapshot.height() / height();
  int w = d->cropRect.width() * d->snapshot.width() / width();
  int h = d->cropRect.height() * d->snapshot.height() / height();

  d->snapshot = d->snapshot.copy(QRect(x, y, w, h));
  d->cropRect = QRect();
  update();
}

void CropWidget::mousePressEvent(QMouseEvent *event)
{
  Q_D(CropWidget);
  if ((!d->_snapped) || (event->button() != Qt::LeftButton))
    return QWidget::mousePressEvent(event);

  const QPoint pos = d->toContents(event->pos());

  d->cropStart = pos;
  event->accept();
}

void CropWidget::mouseMoveEvent(QMouseEvent *event)
{
  Q_D(CropWidget);
  if ((!d->_snapped) || (d->cropStart.isNull()) || (!(event->buttons() & Qt::LeftButton)))
    return QWidget::mouseMoveEvent(event);

  const QPoint pos = d->toContents(event->pos());

  d->cropRect = QRect(d->cropStart, pos).normalized();
  update();
  event->accept();
}

void CropWidget::mouseReleaseEvent(QMouseEvent *event)
{
  Q_D(CropWidget);
  if ((!d->_snapped) || (d->cropStart.isNull()) || (event->button() != Qt::LeftButton))
    return QWidget::mouseReleaseEvent(event);

  const QPoint pos = d->toContents(event->pos());

  d->cropRect = QRect(d->cropStart, pos).normalized();
  update();
  d->cropStart = QPoint();
  event->accept();
}

QSize CropWidget::sizeHint() const
{
  Q_D(const CropWidget);
  if (d->_snapped)
    return QSize(d->snapshot.size().width() + 2 * frameWidth(), d->snapshot.size().height() + 2 * frameWidth());
  else
    return QFrame::sizeHint();
}

QSizePolicy CropWidget::sizePolicy() const
{
  Q_D(const CropWidget);
  if (d->_snapped)
    return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  else
    return QFrame::sizePolicy();
}

QPoint CropWidgetPrivate::toContents(QPoint point) const
{
  Q_Q(const CropWidget);
  QPoint p = point - q->contentsRect().topLeft();
  if (p.x() < 0)
    p.setX(0);
  if (p.x() >= q->contentsRect().size().width())
    p.setX(q->contentsRect().size().width() - 1);
  if (p.y() < 0)
    p.setY(0);
  if (p.y() >= q->contentsRect().size().height())
    p.setY(q->contentsRect().size().height() - 1);
  return p;
}
