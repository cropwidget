/*
  cropwidget
  Copyright (C) 2009  Rafał Rzepecki <divided.mind@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <QDesignerCustomWidgetInterface>
#include <QtPlugin>

#include "cropwidget.h"

class CropWidgetPlugin: public QObject, public QDesignerCustomWidgetInterface
{
  Q_OBJECT
  Q_INTERFACES(QDesignerCustomWidgetInterface)

  public:
    CropWidgetPlugin(QObject *parent = 0): QObject(parent) {}
    QWidget *createWidget(QWidget *parent)
    {
      return new CropWidget(parent);
    }
    QString group() const { return "Input Widgets"; }
    QIcon icon() const { return QIcon(":/icons/transform-crop-and-resize.svg"); }
    QString includeFile() const { return "cropwidget.h"; }
    bool isContainer() const { return true; }
    QString name() const { return "CropWidget"; }
    QString toolTip() const { return "Snapshot and crop widget"; }
    QString whatsThis() const { return "A widget that is able to snapshot and crop its own contents."; }
};

Q_EXPORT_PLUGIN2(cropwidgetplugin, CropWidgetPlugin)
#include "cropwidgetplugin.moc"
