/*
  cropwidget
  Copyright (C) 2009  Rafał Rzepecki <divided.mind@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef CROPWIDGET_H
#define CROPWIDGET_H

#include <QFrame>

class QPaintEvent;
class QMouseEvent;
class QResizeEvent;
class CropWidgetPrivate;

class CropWidget: public QFrame
{
  Q_OBJECT
  Q_PROPERTY(QPixmap snapshot READ snapshot)
  public:
    CropWidget(QWidget *parent = 0);
    virtual ~CropWidget();
    virtual QSize sizeHint() const;
    virtual QSizePolicy sizePolicy() const;

    /**
     * @brief Get the snapshot.
     * @returns pixmap of the snapshot as currently cropped.
     * When not snapped returns an invalid pixmap.
     */
    QPixmap snapshot() const;

  protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

  public slots:
    /**
     * @brief Snap or unsnap an image.
     *
     * When snapped, child widgets are hidden and the snapshot
     * (possibly cropped) is painted.
     */
    virtual void snap(bool snapped = true);
    /**
     * @brief Crop the image.
     *
     * Crop the snapshot image according to the rect user has drawn.
     * No-op when not snapped or no crop rect set.
     */
    virtual void crop();

  private:
    CropWidgetPrivate *d_ptr;
    Q_DECLARE_PRIVATE(CropWidget)
};

#endif
